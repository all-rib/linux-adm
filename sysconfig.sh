#!/bin/bash

#DEBUG=1;

#TODO: test variable with spaces

copy()
{
	FROM="$1";
	TO="$2";
	if [ -a "$FROM" ] || [ -d "$FROM" ]; then
		if [ -n "$DEBUG" ]; then
			echo -e "FUNCTION copy\n";
			echo -e "Copying $FROM to $TO\n";
		else
			cp -r "$FROM" "$TO";
		fi
	else
		echo -e "Cannot copy $FROM. It does not exists\n";
	fi
}

remove()
{
	FILE=$1
	if [ -a "$FROM" ] || [ -d "$FROM" ]; then
			if [ -n "$DEBUG" ]; then
				echo -e "FUNCTION remove\n";
				echo -e "Removing $FILE\n";
		else
			rm -rf "$FILE";
		fi
	else
		echo -e "Cannot remove $FILE. It does not exists\n";
	fi
}

concatenate()
{
	FROM="$1";
	TO="$2";
	OVERWRITE="$3";
	if [ -a "$FROM" ]; then
		if [ -n "$DEBUG" ]; then
			echo -e "FUNCTION concatenate\n";
			if [ -n "$OVERWRITE" ]; then
				echo -e "Overwriting $TO with $FROM\n";
			else
				echo -e "Concatenating $FROM to $TO\n";
			fi
		else
			if [ -n "$OVERWRITE" ]; then
				cat "$FROM" > "$TO";
			else
				cat "$FROM" >> "$TO";
			fi
		fi
	else
		echo -e "Cannot concatenate $FROM. It does not exists\n";
	fi
}

install_package()
{
	PACKAGE="$1"
	if [ -n "$DEBUG" ]; then
		echo -e "FUNCTION install_package\n";
		echo -e "Installing $PACKAGE\n";
	else
		apt -y install "$PACKAGE";
	fi
}

remove_package()
{
	PACKAGE="$1"
	PURGE="$2"
	if [ -n "$DEBUG" ]; then
		echo -e "FUNCTION remove_package\n";
		if [ -n "$PURGE" ]; then
			echo -e "Purging $PACKAGE\n";
		else
			echo -e "Removing $PACKAGE\n";
		fi

	else
		if [ -n "$PURGE" ]; then
			apt -y purge "$PACKAGE";
		else
			apt -y remove "$PACKAGE";
		fi

	fi

}

change_permission()
{
	PERMISSION="$1";
	FILE="$2";
	if [ -n "$DEBUG" ]; then
		echo -e "FUNCTION change_permission\n";
		echo -e "Changing $FILE permissions from $ORIGINAL to $PERMISSION\n";
	else
		chmod "$PERMISSION" "$FILE";
	fi
}

look_in()
{
#	if [ -n "$DEBUG" ]; then
#		echo -e "FUNCTION look_in\n";
#	fi
#Search a file for a specified mac
	mac_list=($(cat "$1"))	#We read the macs, into a vector

	for i in ${mac_list[@]}; do
		if [ -n "$(echo $i | grep ":")" ]; then
			if [ "$mac" = "$i" ]; then #Cut the mac, and compare
				echo "$id" #If found, return the pc number to father function
				exit
			fi
		else
			id=$i
		fi
	done
	echo "0" #Since there's no 0 id, 0 means mac not found
	exit
}

id_mac()
{
#	if [ -n "$DEBUG" ]; then
#		echo -e "FUNCTION id_mac\n";
#	fi
#It's a wrapper. For each room there is a mac list to verify
	for mac in ${macs[@]}; do	#The computer may have more than 1 mac
		for dir in $(ls $WORKDIR); do	#List all rooms
			if [ -a $WORKDIR/$dir/data/macs ]; then	#Take the mac list of the room and search
				number="$(look_in $WORKDIR/$dir/data/macs)"
				if [ "$number" != "0" ]; then
					echo "$dir$number"	#If found, return the PCID
					exit
				fi
			fi
		done
	done
	echo "$DEFAULT_DIR""$( expr $(tail -n 1 $WORKDIR/$DEFAULT_DIR/data/macs | cut -f 1) + 01 )" #Update the machine ID based on unaddressed pcs
	exit
}

room_of()
{
#	if [ -n "$DEBUG" ]; then
#		echo -e "FUNCTION room_of\n";
#	fi
	echo "$( echo $1 | cut -b 1-3 )"	#Extract the room number, given the PCID
}

id_of()
{
#	if [ -n "$DEBUG" ]; then
#		echo -e "FUNCTION id_of\n";
#	fi
	echo "$( echo $1 | cut -b 4-5 )"	#Extract the machine id, given the PCID
}

room_exists()
{
#	if [ -n "$DEBUG" ]; then
#		echo -e "FUNCTION room_exists\n";
#	fi
#Verify the existence of a room
	if [ -d $WORKDIR/"$(room_of $1)" ]; then
		echo "0"
	else
		echo "1"
	fi
	exit
}

validate_pcid()
{
#	if [ -n "$DEBUG" ]; then
#		echo -e "FUNCTION validate_pcid\n";
#	fi
#Validate the PCID. It MUST have 5 numbers, and the room must exist. If not valid, return the default PCID
	validate=$1

	if [ ${#validate} -eq 5 ]; then
		if [ "$(room_exists $( room_of $validate ) )" = "0" ];then
			echo $validate
		else
			echo "$DEFAULT_DIR""$(id_of $validate)"
		fi
	else
		echo "$DEFAULT_DIR""$( expr $(tail -n 1 $WORKDIR/$DEFAULT_DIR/data/macs | cut -f 2) + 01 )" #Update the machine ID based on unaddressed pcs
	fi
}

add_mac()
{
#	if [ -n "$DEBUG" ]; then
#		echo -e "FUNCTION add_mac\n";
#	fi
#Save the machine MAC's in a specified file, by PCID
	if ! [ -d "$WORKDIR/$(room_of $1)" ] ; then
		PCID="$DEFAULT_DIR$( id_of $1 )"
	else
		PCID="$1"
	fi

	ROOM="$(room_of $PCID)"

	for mac in ${macs[@]}; do	#For every mac for this computer, we save each with one numeral
	#	if [ -n "$DEBUG" ]; then
	#		echo -e "Saving $mac with id of $( id_of $PCID ) as a new mac inside $WORKDIR/$ROOM/data/macs\n";
	#	else
			echo -e "$( id_of $PCID )\t$mac" >> $WORKDIR/$ROOM/data/macs
	#	fi
		PCID="$(validate_pcid "$(( 10#$(expr $PCID) + 1 ))")"
	done
}

change_id()
{
#	if [ -n "$DEBUG" ]; then
#		echo -e "FUNCTION change_id\n";
#	fi
#It's a wrapper. After removing a specified mac from a list, adds to another one
	NEW_PCID="$(validate_pcid $1)"
	ROOM=$(room_of $PCID)
	NUMBER=$(id_of $PCID)
	if [ -a $WORKDIR/$ROOM/data/macs ]; then
		index=0
		IFS=$'\n'; #Set white line to \n only
		for i in $(seq 1 $(wc -l $WORKDIR/$ROOM/data/macs | cut -d ' ' -f 1)); do
			lista[index]=$(awk "NR==$i" $WORKDIR/$ROOM/data/macs)
			index=$(( $index + 1 ))
		done

		for mac in ${macs[@]}; do
			index=0
			while [ $index -lt ${#lista[@]} ]; do
				if [ "$mac" = "$(echo ${lista[$index]} | cut -f 2)" ]; then
					lista=( ${lista[@]:0:$index} ${lista[@]:$(( $index + 1 ))} )
					index=$(( $index - 1 ))
				fi
				index=$(( $index + 1 ))
			done
		done

	#	if [ -n "$DEBUG" ]; then
	#		echo -e "Ovewriting ${lista[0]} to $WORKDIR/$ROOM/data/macs\n"
	#		for i in ${lista[@]:2}; do
	#			echo -e "Adding $i to $WORKDIR/$ROOM/data/macs\n"
	#		done
	#	else
			echo ${lista[0]} > $WORKDIR/$ROOM/data/macs
			for i in ${lista[@]:2}; do
				echo $i >> $WORKDIR/$ROOM/data/macs
			done
	#	fi

	fi
	add_mac $NEW_PCID
	echo $NEW_PCID
	exit
}

wait_input()
{
#Waits for input, or continues the procedure
	if [ $1 -ge 0 ]; then
		mp=$(( $TIME*$1 ))
	else
		mp=$TIME
	fi

	read -n 1 -t $mp
	if [ $? == 0 ]; then
		echo "1"
		exit
	fi
	echo "0"
	exit
}

test_hash()
{
	if ! [ -a $LOGDIR/last_good.tar ] && ! [ -a $LOGDIR/hash ]; then
		echo "1"
		exit
	fi

	tar cfv $LOGDIR/verify.tar spec | tee "$LOGDIR/verify.log"	#Generate a new tar to verify the directory
	compromissed=$( sha512sum --quiet -c $LOGDIR/hash )	#If sha512sum retuns something, it's the warning that the file is compromissed

	if [ -z "$compromissed" ]; then	#If the variable is set, the file is compromissed
		mv $LOGDIR/verify.log $LOGDIR/last_good.log
		echo "0"
	else
		echo "1"
	fi
	exit
}

update_hash()
{
#Forces the hash to be updated, as well the backup file
	if [ -n "$DEBUG" ]; then
		echo -e "FUNCTION update_hash\n";
	fi
	tar cfv $LOGDIR/last_good.tar spec/ | tee "$LOGDIR/last_good.log"	#Generate a new tar of the directory
	echo "$(sha512sum $LOGDIR/last_good.tar)" | tee "$LOGDIR/hash"	#Generate and save the new hash
}

test_con()
{
#	if [ -n "$DEBUG" ]; then
#		echo -e "FUNCTION test_con\n";
#	fi
	echo "$(ping -c 1 -q gnu.org >&/dev/null; echo $?)"
}

add_keys()
{
	if [ -n "$DEBUG" ]; then
		echo -e "FUNCTION add_keys\n";
	fi
	remove "/$SSHDIR"
	copy "$WORKDIR/$DEFAULT_DIR/$SSHDIR" "/$SSHDIR"
	
	if [ -n "$ROOM" ]; then       #if there a specific room list, concatenate
		copy "$WORKDIR/$ROOM/$SSHDIR" "/$SSHDIR"
	fi

	change_permission 600 "/$SSHDIR"
	change_permission 640 "/$SSHDIR/authorized_keys"
}

ssh_conf()
{
	if [ -n "$DEBUG" ]; then
		echo -e "FUNCTION ssh_conf\n";
	fi
	copy "$WORKDIR/$DEFAULT_DIR/$SSH" "/$SSH"

	if [ -n "$ROOM" ] && [ -d "$WORKDIR/$ROOM/$SSH" ]; then       #If there's a specific room config file, use the room file
		#TODO: fix this:
		for i in "$WORKDIR/$ROOM/$SSH/*"; do
			concatenate $i "/$SSH"
		done
	fi

	chmod 600 /$SSH

	service sshd --full-restart
}

uptodate()
{
	if [ -n "$DEBUG" ]; then
		echo -e "FUNCTION uptodate\n";
	fi
	#Update the computer, duh!

	apt -y update
	apt -y full-upgrade
	apt -y autoremove
}

build_lists()
{
	if [ -n "$DEBUG" ]; then
		echo -e "FUNCTION build_lists\n";
	fi
#Building apps installation and removal lists

	echo "Building apps lists"

	concatenate "$WORKDIR/$DEFAULT_DIR/$INSTALL" "/tmp/prog_tmp" #Save the general apps list
	concatenate "$WORKDIR/$DEFAULT_DIR/$REMOVE" "/$REMOVE" #Read the to-remove default list

	if [ -n "$ROOM" ]; then       #if there a specific room list, concatenate
		concatenate "$WORKDIR/$ROOM/$INSTALL" "/tmp/prog_tmp"
		concatenate "$WORKDIR/$ROOM/$REMOVE" "/$REMOVE"
	fi

	cat /tmp/prog_tmp | grep -v "$(cat /$REMOVE)" > "/$INSTALL" #Remove the to-remove apps from install list, due possible colision problems

	echo "Uninstalling unwanted packages"
	for i in $(cat /$REMOVE); do
		remove_package "$i" PURGE;
	done

	#Installing listed programs in prog_ins

	echo "Installing wanted packages"
	for i in $(cat /$INSTALL); do
		install_package "$i";
	done

	rm "/$INSTALL" #clean up the mess
	rm "/$REMOVE"
	rm /tmp/prog_tmp
}

#ambient_conf()
#{
#	# Configuration for automatic login. EXPERIMENTAL
#
#	GDM=$(ls /etc/ | grep "gdm")
#
#	for i in $GDM; do
#		echo -e "# GDM configuration storage\n\
##\n\
## See /usr/share/gdm/gdm.schemas for a list of available options\n\
#\n\
#[daemon]\n\
## Uncoment the line velow to force the login screen to use Xorg\n\
##WaylandEnable=false\n\
#\n\
## Enabling automatic login\n\
#AutomaticLoginEnable = true\n\
#AutomaticLogin = $LUSER\n\
#\n\
## Enabling timed login\n\
## TimedLoginEnable = true\n\
## TimedLogin = $LUSER\n\
## TimedLoginDelay = 10\n\
#\n\
#[security]\n\
#\n\
#[xdmcp]\n\
#\n\
#[chooser]\n\
#\n\
#[debug]\n\
## Uncoment the line below to turn on debbuging\n\
## Enable=true\n" > "/etc/$i/custom.conf"
#	done
#}

incognito_conf()
{
	if [ -n "$DEBUG" ]; then
		echo -e "FUNCTION incognito_conf\n";
	fi
#Installation of incognito script for aluno incognito user
	echo "Time tavel machine time (incognito script)..."

	copy "$WORKDIR/$DEFAULT_DIR/$SCRIPT" "/$SCRIPT"

	copy "$WORKDIR/$DEFAULT_DIR/etc/skel/*" "/etc/skel"

	if [ -n "$ROOM" ]; then	#If there's a specific room config file, use the room file
		concatenate "$WORKDIR/$ROOM/$SCRIPT" "/$SCRIPT"
		copy "$WORKDIR/$ROOM/$SERVICE" "/$SERVICE"
	else
		concatenate "$WORKDIR/$DEFAULT_DIR/$SCRIPT" "/$SCRIPT"
		copy "$WORKDIR/$DEFAULT_DIR/$SERVICE" "/$SERVICE"
	fi

	change_permission 450 "/$SCRIPT"	#Change files permission to avoid unwanted access
	change_permission 440 "/$SERVICE"

	if [ "$(systemctl is-enabled incognito)" == "enabled" ]; then
		if [ -n "$DEBUG" ]; then
			echo -e "Disabling and enabling incognito again\n";
		else
			systemctl disable incognito.service	#Reset the Daemon for reseting user script
			systemctl enable incognito.service
		fi
	fi
}


hostname_conf()
{
	if [ -n "$DEBUG" ]; then
		echo -e "FUNCTION hostname_conf\n";
		if [ $( expr $PCID ) -lt 20000 ]; then	#If PCID it's not de default, defines the machine hostname as this.
			echo -e "Changing your hostname to v$ROOM-$(id_of $PCID)";
		else
			echo -e "Not changing your hostname: PCID is not less than 20000";
		fi
	else
		if [ $( expr $PCID ) -lt 20000 ]; then	#If PCID it's not de default, defines the machine hostname as this.
			hostnamectl set-hostname "V$ROOM""M$(id_of $PCID)"
		fi
	fi
}

post_clear()
{
	if [ -n "$DEBUG" ]; then
		echo -e "FUNCTION post_clear\n";
	fi
#Clear for-support installed apps, to avoid unecessary apps

	concatenate "$WORKDIR/$DEFAULT_DIR/tmp/prog_aft" "/tmp/prog_aft" OVERWRITE
	if [ -n "$ROOM" ]; then
		concatenate "$WORKDIR/$ROOM/tmp/prog_aft" "/tmp/prog_aft";
	fi

	for i in $(cat /tmp/prog_aft); do
		remove_package "$i";
	done

	apt -y autoremove
	apt -y clean
}

grubify()
{
	if [ -n "$DEBUG" ]; then
		echo -e "FUNCTION grubify\n";
	fi
# TODO: See if detects Windows

#Update grub config files
	if [ -n "$ROOM" ]; then
		copy "$WORKDIR/$ROOM/$GRUB" "/$GRUB"
	else
		copy "$WORKDIR/$DEFAULT_DIR/$GRUB" "/$GRUB"
	fi

	remove "10_linux_zfs"
	remove "/etc/grub.d/20*"
	remove "/etc/grub.d/30_uefi-firmware"
	remove "/etc/grub.d/4*"

	if [ -n "$DEBUG" ]; then
		echo -e "Regenerating /boot/grub/grub.cfg\n";
	else
		grub-mkconfig -o /boot/grub/grub.cfg
		update-grub
	fi
}

logrotate_conf()
{
	if [ -n "$DEBUG" ]; then
		echo -e "FUNCTION logrotate_conf\n";
	fi

	copy "$WORKDIR/$DEFAULT_DIR/$LOGROTATE" "/$LOGROTATE"
	if [ -n "$ROOM" ]; then
		concatenate "$WORKDIR/$ROOM/$LOGROTATE" "/$LOGROTATE"
	fi
}

package_conf()
{
	if [ -n "$DEBUG" ]; then
		echo -e "FUNCTION package_conf\n";
	fi

	echo "installing pre-compiled apps"
	if [ -a "$WORKDIR/$ROOM/scripts/prec_ins.sh" ]; then
		source "$WORKDIR/$ROOM/scripts/prec_ins.sh"
	else
		source "$WORKDIR/$DEFAULT_DIR/scripts/prec_ins.sh" "$WORKDIR"
	fi
	precomp
}

finish()
{
	if [ -n "$DEBUG" ]; then
		echo -e "FUNCTION finish\n";
	fi

	#Removing installed suport programs, they're not needed anymore, listed in prog_aft

	echo "Removing apps installed due script operations"
	post_clear

	echo "After job's for this room, if any (specifc room scripts)"
	if [ -n "$ROOM" ] && [ -a $WORKDIR/$ROOM/scripts/after.sh ]; then
		source "$WORKDIR/$ROOM/scripts/after.sh"
	fi
	action
	uptodate
}

find_impostors()
{
#Serach for non-default users on the computer, and delete them

	if [ -n "$DEBUG" ]; then
		echo -e "FUNCTION find_impostors\n";
	fi
	users="$(compgen -u)" #List the users names
	for user in $users; do
		if [ "$user" != "$LUSER" ] || [ "$user" != "$AUSER" ]; then
			useruid="$(id -u $user)" #Get the uid of each user
			if [ $useruid -gt 1000 ]; then #it means i'ts not a system/default user
				if [ -n "$DEBUG" ]; then
					echo -e "$user would be deleted\n";
				else
					userdel -r $user
				fi
			fi
		fi
	done
}

user_conf()
{
	if [ -n "$DEBUG" ]; then
		echo -e "FUNCTION user_conf\n";
	fi

	#Add the admin user and remove default unwanted users permissions
	echo "You may pass (adding users)"

	find_impostors

	if [ -n "$DEBUG" ] && [ "$DEBUG" == 1 ]; then
		echo -e "Just to be sure, i'll reset users passwords for you :)\n"
		echo -e "Writing $SHADOW, without $AUSER and $LUSER lines to /tmp/shadow\n"

		echo -e "Creating $LUSER user with home and /bin/bash as shell\n"
		echo -e "Password deletion for $LUSER\n"

		if ! [ -z "$LUSER_P" ]; then
			echo -e "$LUSER:$LUSER_P:0:99999:99999:::: >> /tmp/shadow\n";
		fi

		# Administrador add
		echo -e "Creating $AUSER user without home, and /bin/bash as shell\n"
		echo -e "Password deletion for $AUSER\n"

		if ! [ -z "$AUSER_P" ]; then
			echo -e "$AUSER:$AUSER_P:0:99999:99999:::: >> /tmp/shadow\n";
		fi

		# Root add
		# TODO: Find a way to keep persons away from logging into root
		if [ -z "$ROOT_P" ]; then
			echo "Leaving root password as it is"
		else
			echo -e "root:$ROOT_P:0:99999:99999:::: >> /tmp/shadow\n";
		fi

		# Move shadow back

		echo -e "Overwriting original shadow\n";
		change_permission 640 "/$SHADOW"

		# Clean sudoers
		
		echo -e "Writing $SUDOERS, without %sudo and %admin lines to /tmp/sudoers\n"
		concatenate "$WORKDIR/$DEFAULT_DIR/$SUDOERS" "/tmp/sudoers"

		if [ "$ROOM" != "$DEFAULT_DIR" ]; then
			concatenate "$WORKDIR/$ROOM/$SUDOERS" "/tmp/sudoers"
		fi

		echo -e "Overwriting $SUDOERS with /tmp/sudoers\n";

		#TODO: If there's no such group, it returns a warning. Remove it
		echo -e "deleting sudo group\n";
		echo -e "deleting admin group\n";
	else

		#Add the admin user and remove default unwanted users permissions
		echo "You may pass (adding users)"

		echo "Just to be sure, i'll reset users passwords for you :)"
		cp "/$SHADOW" "/$SHADOW.old"
		cat "/$SHADOW" | grep -v root | grep -v "$AUSER" | grep -v "$LUSER" >> /tmp/shadow

		# Aluno add

		useradd -m -s /bin/bash -p $LUSER_P $LUSER

		df="$(echo $(($(date --utc --date "$1" +%s)/86400)))";

		# Administrador add
		useradd -r -s /bin/bash -p $AUSER_P $AUSER

		echo -e "[User]\nLanguage=\nXSession=gnome\nSystemAccount=true" > /var/lib/AccountsService/users/$AUSER

		# Root add
		# TODO: Find a way to keep persons away from logging into root
		if [ -z "$ROOT_P" ]; then
			echo "Leaving root password as it is"
		else
			echo "root:$ROOT_P:$df:99999:99999::::" >> /tmp/shadow
		fi

		# Move shadow back

		mv /tmp/shadow "/$SHADOW"
		chmod 640 "/$SHADOW"

		# Clean sudoers

		cat "/$SUDOERS" | grep -v %sudo | grep -v %admin > /tmp/sudoers
		concatenate "$WORKDIR/$DEFAULT_DIR/$SUDOERS" "/tmp/sudoers"

		if [ -n "$ROOM" ]; then
			concatenate "$WORKDIR/$ROOM/$SUDOERS" "/tmp/sudoers"
		fi

		mv /tmp/sudoers "/$SUDOERS"

		#TODO: If there's no such group, it returns a warning. Remove it
		#groupdel -f sudo
		#groupdel -f admin

		#if [ -z "$LUSER_P" ]; then
		passwd -d -u "$LUSER"
			#echo "$LUSER:$LUSER_P:$df:99999:99999::::" >> /tmp/shadow
			#passwd -x -1 "$LUSER"
		#fi

		#if ! [ -z "$AUSER_P" ]; then
			#passwd -d -u "$AUSER"
			#echo "$AUSER:$AUSER_P:$df:99999:99999::::" >> /tmp/shadow
			#passwd -x -1 "$LUSER"
		#fi



	fi
}

goodbye()
{
#GOOD-FCKN-BYE
	echo "Shuting down... press any buton to menu"
	if [ "$(wait_input 100)" = "1" ]; then
		help
		read op
		menu ${op[@]}
		exit
	fi

	echo "Goodbye :)"
	shutdown -Ph now
}

hello()
{
	echo -e "Hello, script!"
	echo -e "Computer configuration script, by Allan Ribeiro"
	echo -e "email to:<allanribeiro<at>alunos.utfpr.edu.br>\n"
}

help()
{

	echo -e "
	--info			Returns info about this pc identification (uses mac)\n
	--integrity-check	Check whether the program has been altered\n
	--update-hash		Update program hash\n
	--change-pcid		Change the current pcid (changes mac lists as well)\n
	--add-mac		Change the mac of a given PCID\n
	--user-config		Run the users configuration functions\n
	--default-programs	Install the default programs to the given PCID, and update them\n
	--incognito		Install the incognito service\n
	--logrotate		Do the logging configuration\n
	--hostname		Change the hostname accordingly to PCID\n
	--ssh-config		Install SSH keys and server\n
	--grubify		Regenerate grub config files accordingly to PCID\n
	--after-jobs		Install programs which are not in APT\n
	--hello			Show the hello message\n
"

}

menu()
{
	source $WORKDIR/$DEFAULT_DIR/variables
	if [ "$ROOM" != "$DEFAULT_DIR" ] && [ -a $WORKDIR/$ROOM/variables ]; then
		source $WORKDIR/$ROOM/variables
	fi

	for parameter in $@; do
		case $parameter in
			--help )
				help;
				;;

			--info )
				echo -e "The current PCID is $PCID";
				exit
				;;
			--integrity-check )
				if [ $(test_hash) != 0 ]; then
					echo -e "A file has been modified. Consider forcing the hash to change\n"
					echo -e "--update-hash option, btw\n"
				else
					echo "No alterations has been found."
				fi
				;;
			--update-hash )
				update_hash
				;;
			--change-pcid )
				echo "Type the PCID in xyzab format (ex. 00120 for room 001, machine 20)"
				read NEW_PCID
				if ! [ -d $WORKDIR/"$(room_of $PCID)" ]; then
					NEW_PCID="$DEFAULT_DIR""$(id_of $PCID)"
				fi
				ROOM=$(room_of $NEW_PCID)
				source $WORKDIR/$ROOM/variables
				;;
			--add-mac )
				change_id
				;;
			--user-config )
				user_conf
				;;
			--default-programs )
				uptodate
				build_lists
				package_conf
				;;
			--incognito )
				incognito_conf
				;;
			--logrotate )
				logrotate_conf
				;;
			--hostname )
				hostname_conf
				;;
			--ssh-config )
				ssh_conf
				;;
			--grubify )
				grubify
				;;
			--after-jobs )
				finish
				;;
			--hello )
				hello
				;;
			* )
				echo "Invalid parameter"
				help
				exit
		esac
	done
}

default_procedure()
{
	#nmcli n connectivity check	#If not logged in, force the login screen to appear

	if [ "$(test_con)" -ne 0 ]; then
		echo "Internet not working. Exiting"
		exit	
	fi

	source "$WORKDIR/$DEFAULT_DIR/variables"
	if [ -n "$ROOM" ] && [ -a "$WORKDIR/$ROOM/variables" ]; then
		source "$WORKDIR/$ROOM/variables"
	fi

	# Program flags setting

	if [ -n "$LOGDIR" ] && ! [ -d "$LOGDIR" ]; then
		mkdir "$LOGDIR"
	fi

	#Users setting

	echo "Configuring users"
	user_conf

	echo "Checking apt. Hope you're not running it"

	if [ "$(fuser /var/lib/dpkg/lock)" ]; then
		echo "APT running somewhere. Stop it first"
		ps -e | grep "apt"
		ps -e | grep "dpkg"
		echo -e "This might help you... Use \"kill $PROCCESS_NUMBER\" (first number)"		
		exit
	fi

	if [ "$(uname -v | cut -d ' ' -f 3)" == "Debian" ]; then
		mv /etc/apt/sources.list /etc/apt/sources.old;
		echo -e "deb http://deb.debian.org/debian bullseye main non-free\ndeb-src http://deb.debian.org/debian bullseye main non-free\ndeb http://deb.debian.org/debian-security/ bullseye-security main non-free\ndeb-src http://deb.debian.org/debian-security/ bullseye-security main non-free\ndeb http://deb.debian.org/debian bullseye-updates main non-free\ndeb-src http://deb.debian.org/debian bullseye-updates main non-free\n" > /etc/apt/sources.list
	fi

	#Updating and installing needed packages

	echo "Updating system"
	uptodate

	#Building apps installation and removal lists

	echo "Building apps list"
	build_lists	

	# TODO: Put this line in a better place:
	rm -rf /usr/share/applications/nethack*

	echo "Now, configuring apps"
	package_conf

	#Installation of incognito script for aluno incognito user

	echo "Time travel machine time (incognito script)..."
	incognito_conf

	#Hostname configuration

	echo -e 'Changing the machine hostname'
	hostname_conf

	#GRUB reconfiguration

	echo "Reconfiguring grub"
	grubify

	#Configure ambient

	#ambient_conf

	# configure ssh-server

	ssh_conf

	# add dainf's key

	add_keys

	#Finishing

	echo "last package verification before remove support-to-script apps"
	finish

	#goodbye
}

main()
{
	if [ "$(whoami)" = "root" ]; then #Check if the user has the permission to run this script

		WORKDIR="$(pwd)/spec"	#Path to room's folder
		DEFAULT_DIR="200"	#If a room wasn't selected, the default room is used
		LOGDIR="$(pwd)/tmp"
		TIME=5

		hello	#Say hello, Script
	
		macs=($(for i in /sys/class/net/*; do cat $i/address; done))	#Get the computer mac's
		PCID="$(id_mac)"	#Try to identify the mac
		ROOM="$(room_of $PCID)"	#Save the room number for the functions

		if [ "$ROOM" == "$DEFAULT_DIR" ]; then
			unset ROOM;
		fi

		if ! [ "$#" -gt 0 ]; then #Check for parameters (specifc script functions). If no parameters has been given, run program normally
			echo "PCID is $PCID. Is this right? Press any key, if not"	#In case of the user want to change the mac identity
			if [ "$(wait_input 1)" = "1" ]; then
				echo "The format of PCID is ROOMMACHINE, as 00902 for room 009 and machine 02"
				echo "If the id is invalid, the mac saved in $DEFAULT_DIR room, and the default instalation will follow"
				echo "Type the PCID"
				read NEW_PCID
				PCID=$(change_id $NEW_PCID)
				ROOM="$(room_of $PCID)"	#Save the room number for the functions
				if [ "$ROOM" == "$DEFAULT_DIR" ]; then
					unset ROOM;
				fi
			fi

			default_procedure

		else #Call specifc script funtions
			menu $@
		fi
	else
		echo "Thanks for avoiding root. But this script needs it (try with sudo)"
	fi
}

export PATH=/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin

main $@ 2>&1 | tee tmp/main.log

