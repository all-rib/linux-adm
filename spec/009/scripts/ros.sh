ros_install()
{
	echo "installing"

	sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
	curl -sSL 'http://keyserver.ubuntu.com/pks/lookup?op=get&search=0xC1CF6E31E6BADE8868B172B4F42ED6FBAB17C654' | sudo apt-key add -
	sudo apt update

	for i in $(cat $WORKDIR/$ROOM/scripts/ros/list); do
		sudo apt-get install -y $i
	done

	sudo rosdep init
	sudo rosdep fix-permissions
	rosdep update

	echo "finish install"

	#Create workspace

	cp /home/$LUSER/.bashrc /tmp
	cp $WORKDIR/$ROOM/script/ros/.bashrc /home/$LUSER/.bashrc

	source /home/$LUSER/.bashrc

	mkdir -p /home/$LUSER/catkin_ws/src
	cd /home/$LUSER/catkin_ws/
	catkin_make

	# ------------ Extra ------------------------

	# Turtlebot3
	echo "Turtlebot3"
	wget https://raw.githubusercontent.com/ROBOTIS-GIT/robotis_tools/master/install_ros_melodic.sh
	chmod 755 ./install_ros_melodic.sh
	bash ./install_ros_melodic.sh

	cd /home/$LUSER/catkin_ws/src/
	git clone https://github.com/ROBOTIS-GIT/turtlebot3_msgs.git
	git clone https://github.com/ROBOTIS-GIT/turtlebot3.git
	cd /home/$LUSER/catkin_ws
	catkin_make

	# Simulator
	echo "Turtlebot3 Simulator"
	cd /home/$LUSER/catkin_ws/src/
	git clone https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git
	cd /home/$LUSER/catkin_ws
	catkin_make

	cd /home/$LUSER/catkin_ws/src/
	git clone https://github.com/ROBOTIS-GIT/turtlebot3_gazebo_plugin
	cd /home/$LUSER/catkin_ws
	catkin_make

	sudo rosdep fix-permissions
	sudo -u rosdep update

	echo "$( cat /home/$LUSER/.bashrc | grep -A 40 ROS_VARIABLES )" >> /tmp/.bashrc
	mv /tmp/.bashrc /home/$LUSER
}
