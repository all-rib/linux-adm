#!/bin/bash

action()
{
	mkdir /mnt/storage
	mount /dev/sda1 /mnt/storage
	ln -s /mnt/storage "/home/$LUSER/$LUSER"
	cp /etc/fstab /etc/fstab.old
	cat /etc/fstab | grep -v "/mnt/storage" >> /tmp/fstab
	echo -e "/dev/sda1\t/mnt/storage\text4\trw,nosuid,nodev,relatime\t0\t2" >> /tmp/fstab
	sudo -u "$LUSER" xset s noexpose
	sudo -u "$LUSER" xset s noblank
	sudo -u "$LUSER" xset s off
	chmod 777 -R "/home/$LUSER/$LUSER"
}
