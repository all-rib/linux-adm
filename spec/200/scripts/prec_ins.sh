#!/bin/bash


fix_dpkg()
{
	apt --fix-broken -y install
}

prereq()
{
	for i in $(cat $1.txt); do
		install_package $i &>> "$LOGDIR/$1.txt"
	done
}

install()
{
	if [ -n $DEBUG ]; then	
		echo -e "Running dpkg for $1.deb\n";
	else
		dpkg --install "$1.deb"
	fi
}

handle()
{
	prereq $1
	install $1
}

packet-tracer()
{
	PACKAGE="packet-tracer"

	if [ -n $DEBUG]; then
		echo -e "$PACKAGE is being installed";
		echo -e "FOLDER=\"$WORKDIR/$DEFAULT_DIR/scripts/$PACKAGE\""
	fi

	FOLDER="$WORKDIR/$DEFAULT_DIR/scripts/$PACKAGE"
	handle "$FOLDER"

	unset PACKAGE
	unset FOLDER
}

atom()
{
	PACKAGE="atom"
	FOLDER="$WORKDIR/$DEFAULT_DIR/scripts/$PACKAGE"
	VERSION='v1.60.0'
	LINK="https://github.com/atom/atom/releases/download/$VERSION/atom-amd64.deb"

	if [ -n $DEBUG]; then
		echo -e "$PACKAGE is being installed";
		echo -e "FOLDER=\"$WORKDIR/$DEFAULT_DIR/scripts/$PACKAGE\""
	else
		wget "$LINK" -O "$FOLDER.deb"
	fi

	handle $FOLDER

	unset PACKAGE
	unset FOLDER
	unset VERSION
	unset LINK
}

virtual-box()
{
	PACKAGE="virtual-box"
	FOLDER="$WORKDIR/$DEFAULT_DIR/scripts/$PACKAGE"
	VERSION='6.1_6.1.26-145957'
	LINK="https://download.virtualbox.org/virtualbox/6.1.26/virtualbox-$VERSION~Ubuntu~eoan_amd64.deb"

	if [ -n $DEBUG]; then
		echo -e "$PACKAGE is being installed";
		echo -e "FOLDER=\"$WORKDIR/$DEFAULT_DIR/scripts/$PACKAGE\""
	else
		wget "$LINK" -O "$FOLDER.deb"
	fi

	handle $FOLDER

	unset PACKAGE
	unset FOLDER
	unset VERSION
	unset LINK
}

arduino()
{
	PACKAGE="arduino"
	FOLDER="$WORKDIR/$DEFAULT_DIR/scripts/$PACKAGE"
	VERSION='1.8.15'
	LINK="https://downloads.arduino.cc/arduino-$VERSION-linux64.tar.xz"

	if [ -n $DEBUG]; then
		echo -e "$PACKAGE is being installed";
		echo -e "FOLDER=\"$WORKDIR/$DEFAULT_DIR/scripts/$PACKAGE\""
	else
		wget "$LINK" -O "$FOLDER.tar.xz"
		tar -xf "$FOLDER.tar.xz"
		mv "$PACKAGE-$VERSION" /usr/share
		cd "/usr/share/$PACKAGE-$VERSION"
		./install.sh &>> "$LOGDIR/PACKAGE.txt"
		cd -
		adduser $LUSER dialout
		rm "$FOLDER.tar.gz"
	fi

	unset PACKAGE
	unset FOLDER
	unset VERSION
	unset LINK
}

python()
{
	PACKAGE="Python"
	FOLDER="$WORKDIR/$DEFAULT_DIR/scripts/$PACKAGE"
	VERSION='3.9.6'
	LINK="https://www.python.org/ftp/python/$VERSION/Python-$VERSION.tar.xz"

	if [ -n $DEBUG]; then
		echo -e "$PACKAGE is being installed";
		echo -e "FOLDER=\"$WORKDIR/$DEFAULT_DIR/scripts/$PACKAGE\""
	else
		wget "$LINK" -O "$FOLDER.deb"
		wget "$LINK" -O "$FOLDER.tar.xz"
		tar -xf "$FOLDER.tar.xz"
		cd "$PACKAGE-$VERSION"
		./configure &>> "$LOGDIR/$PACKAGE.txt"
		make &>> "$LOGDIR/$PACKAGE.txt"
		make install &>> "$LOGDIR/$PACKAGE.txt"
		cd -
	fi

	unset PACKAGE
	unset FOLDER
	unset VERSION
	unset LINK
}

precomp()
{
	#python
	#atom
	#arduino
	#virtual-box
	#packet-tracer
}
