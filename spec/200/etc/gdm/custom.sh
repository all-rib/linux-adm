echo -e "# GDM configuration storage\n\
#\n\
# See /usr/share/gdm/gdm.schemas for a list of available options\n\
\n\
[daemon]\n\
# Uncoment the line velow to force the login screen to use Xorg\n\
#WaylandEnable=false\n\
\n\
# Enabling automatic login\n\
AutomaticLoginEnable = true\n\
AutomaticLogin = hashed\n\
\n\
# Enabling timed login\n\
# TimedLoginEnable = true\n\
# TimedLogin = hashed\n\
# TimedLoginDelay = 10\n\
\n\
[security]\n\
\n\
[xdmcp]\n\
\n\
[chooser]\n\
\n\
[debug]\n\
# Uncoment the line below to turn on debbuging\n\
# Enable=true\n"
	
